////
//  DropDown.m
//  WiFIScan
//
//  Created by Armen on 6/20/14.
//  Copyright (c) 2014 Armen. All rights reserved.
//


#import "QuartzCore/QuartzCore.h"
#import "DropDownView-1.h"
//#import "DropDownView.h"
//#import "ProfileViewController.h"


@interface DropDownView() <UIApplicationDelegate>
{
    CGRect btn ;
    AppDelegate* appObj;
    //ProfileViewController * profileView;
}
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property(nonatomic, retain) NSArray *imageList;
//@property (strong , nonatomic) ProfileViewController * mPrifileObj;

@end

@implementation DropDownView

@synthesize table;
@synthesize btnSender;
@synthesize list;
@synthesize imageList;
@synthesize delegate;
@synthesize animationDirection;

- (id)showDropDown:(UIButton *)b height:(CGFloat *)height array:(NSArray *)arr direction:(NSString *)direction {
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
   // CGFloat screenHeight = screenRect.size.height;
    //profileView = [[ProfileViewController alloc] init];
    btnSender = b;
    animationDirection = direction;
       self.table = (UITableView *)[super init];
    
    if (self) {

         btn = b.frame;
        self.list = [NSArray arrayWithArray:arr];
        
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(0, -5);
        } else if ([direction isEqualToString:@"down"]) {

            self.frame = CGRectMake(btn.size.width/2, btn.origin.y+btn.size.height+5, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(0, 5);
        }
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 100, 0)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        table.backgroundColor = [UIColor whiteColor];//[UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:1];
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        table.separatorColor = [UIColor grayColor];
       // CGFloat dropWidh = self.frame.size.width - 100;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x - 40, btn.origin.y - *height, btn.size.width + 70, *height);
            table.frame = CGRectMake(0, 0, screenWidth - 170, *height);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, *height);
            table.frame = CGRectMake(0, 0, btn.size.width, *height);
        }
        
        [UIView commitAnimations];
        [b.superview addSubview:self];
       // profileView.navigationItem.accessibilityFrame = CGRectMake(0, 0, 320, 100);
        [self addSubview:table];
        
        
    }
    return self;
}
-(void)reloadData :(NSMutableArray*)data{
    CGFloat f ;
    if(data.count > 5){
        f = 5 * 40.f;
    }else{
        f = data.count * 40.f;
    }

    self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, f);
    table.frame = CGRectMake(0, 0, btn.size.width, f);
    self.list = data;
    [self.table reloadData];
    
}
- (void)hideDropDown:(UIButton *)b {
    
    CGRect btn = b.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if ([animationDirection isEqualToString:@"up"]) {
        

        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        


        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
    }
    
    cell.textLabel.text = [list objectAtIndex:indexPath.row];
    //cell.textLabel.alpha = 0.7f;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self hideDropDown:btnSender];
    
    //_mPrifileObj = [[ProfileViewController alloc] init];
    //UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //[btnSender setTitle :cell.textLabel.text forState:UIControlStateNormal];
    
        appObj = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appObj.selectedIndex = indexPath.row;
  //  appObj.list = self.list.count;
  
    [self myDelegate];
    
}

- (void) myDelegate {
    [self.delegate dropDownDelegateMethod:self];
}


@end
